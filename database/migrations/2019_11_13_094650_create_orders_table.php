<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('seller_id')->unsigned();
            $table->foreign('seller_id')->references('seller_id')->on('sellers');
            $table->integer('buyer_id')->unsigned();
            $table->foreign('buyer_id')->references('buyer_id')->on('buyers');
            $table->string('payment_method');
            $table->string('delivery_method');
            $table->date('order_date');
            $table->date('delivery_date');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
