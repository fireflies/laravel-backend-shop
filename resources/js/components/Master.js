import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import Register from './Register';
import Login from './Login';
import ReactDOM from 'react-dom';

export default class Master extends Component {
  render(){
    return (
      <Router>
        <div>
            <h2>Welcome to SFTech website</h2>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <ul className="navbar-nav mr-auto">
                  <li><Link to={'/'} className="nav-link"> Home</Link></li>
                  <li><Link to={'/register'} className="nav-link"> register</Link></li>
                  <li><Link to={'/about'} className="nav-link"> About</Link></li>
                  <li><Link to={'/Login'} classname='nav-link'> Login</Link></li>
                  {/* {this.props.children} */}
              </ul>
            </nav>
            <hr />


            <Switch>
              <Route exact path='/register' component={Register} />
              {/* <Route exact path='/about' component={About} />               */}
              <Route exact path='/Login' component={Login} />
            </Switch>
            

        </div>
        </Router>
    )
  }
}

ReactDOM.render(<Master />, document.getElementById('example'));
