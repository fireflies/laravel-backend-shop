// CreateItem.js

import React, {Component} from 'react';

class Register extends Component {
  constructor(){

    this.setState = {name: ''};

    
  }
  changeHandle = (event) => {
    // console.log('hello');

    this.setState({name: 'hello'});
    console.log(this.setState.name);
  }
    render() {
      return (
      <div>
        <h1>Login to Shoppee</h1>
      {/* <h1>{this.setState.firstname}</h1> */}
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>First name:</label>
                  <input type="text" name="firstname" className="form-control" onChange={this.changeHandle}/>
                </div>
              </div>
              </div>
              <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Middle name:</label>
                  <input type="text" name="middlename" className="form-control" onChange={this.changeHandle}/>
                </div>
              </div>
              </div>
              <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Last name:</label>
                  <input type="text" name="lastname" className="form-control"  onChange={this.changeHandle}/>
                </div>
              </div>
              </div>
              <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Email:</label>
                  <input type="email" name="email" className="form-control" onChange={this.changeHandle}/>
                </div>
              </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>password:</label>
                    <input type="password" name="password" className="form-control col-md-6" onChange={this.changeHandle}/>
                  </div>
                </div>
              </div><br />
              <div className="form-group">
                <button className="btn btn-primary" type='submit'>Register</button>
              </div>
          </form>
    </div>)
    }
}
export default Register;