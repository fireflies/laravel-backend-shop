<!-- welcome.blade.php -->

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>shoppee</title>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/meanmenu.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/ionicons.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/nivo-slider.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/responsive.css')}}" rel="stylesheet" type="text/css">   
        <link href="{{asset('css/strap.css')}}"  rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="example"></div>
        <script src="{{asset('js/app.js')}}" ></script>
    </body>
</html>